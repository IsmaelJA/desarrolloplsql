/*Cursores impl�citos: definidos autom�ticamente por Oracle para todas las 
sentencias DML de SQL (INSERT, UPDATE, DELETE y MERGE), y para las sentencias 
SELECT que devuelven solo una fila.
Cursores expl�citos: declarados por el programador para consultas que devuelven 
m�s de una fila.
Puede usar cursores expl�citos para nombrar un �rea de contexto y acceder a sus 
datos almacenados
*/

DECLARE
    CURSOR cur_depts IS -- Se define el nombre del cursor
    SELECT                 -- El cursor es una variable que soporta almacenar una lista/un 
        department_id,      -- conjunto de registros
        department_name,
        location_id
    FROM
        departments;

    v_department_id    departments.department_id%TYPE;
    v_department_name  departments.department_name%TYPE;
    v_location_id       departments.location_id%TYPE;
BEGIN
    OPEN cur_depts;  -- Se ejecut el cursor
    LOOP  -- Se itera el cursor 
            -- Un cursor siempre se itera 
    /*
    FETCH: instrucci�n que recupera la fila actual y avanza el cursor a la siguiente 
    fila hasta que no haya m�s filas o hasta que se cumpla una condici�n espec�fica.
    */
        FETCH cur_depts INTO 
            v_department_id,
            v_department_name,
            v_location_id;
        EXIT WHEN cur_depts%notfound; -- Sale del loop cuando ya no haya mas registros
        dbms_output.put_line(v_department_id
                             || ' '
                             || v_department_name)
                             || ' '
                             || v_location_id);
    END LOOP;

    CLOSE cur_depts;
END;