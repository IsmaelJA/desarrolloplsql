/*
Cursor explicito con FOR

FOR record_name IN cursor_name LOOP
statement1;
statement2;
. . .
END LOOP;

- record_name Is the name of the implicitly declaredrecord(as cursor_name%ROWTYPE)
- cursor_name Is a PL/SQL identifier for a previouslydeclared cursor
*/

-- Se sintetiza el codigo y el DECLARE se realiza en un subquery 
BEGIN
    FOR v_emp_record IN (
        SELECT
            employee_id,
            last_name
        FROM
            employees
        WHERE
            department_id = 50
    ) LOOP
        dbms_output.put_line('ID de empleado: '||v_emp_record.employee_id
                             || ' '
                             ||'Apellido: ' || v_emp_record.last_name);
    END LOOP;
END;