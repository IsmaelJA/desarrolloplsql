/*
Funciones 
Una funci�n es un bloque PL / SQL con nombre (subprograma) que puede 
aceptar par�metros IN opcionales y debe devolver exactamente un valor.
En las expresiones SQL, una funci�n debe obedecer reglas espec�ficas 
para controlar los efectos secundarios.
Evite lo siguiente dentro de las funciones:
-Cualquier tipo de DML o DDL
-COMMIT O ROLLBACK
-Alterar variables globales

CREATE [OR REPLACE] FUNCTION function_name
[(parameter1 [mode1] datatype1, ...)]
RETURN datatype IS|AS
[local_variable_declarations; �]
BEGIN
-- actions;
RETURN expression;
END [function_name];

El encabezado de una funci�n es como un encabezado de 
PROCEDURE con dos diferencias:
-El modo de par�metro solo debe ser IN.
-La cl�usula RETURN se usa en lugar del modo OUT.

*/

-- La funcion obtiene el salario, tomando como parametro el employee_id
CREATE OR REPLACE FUNCTION get_sal (
    p_id IN employees.employee_id%TYPE
) RETURN NUMBER IS
    v_sal employees.salary%TYPE := 0;
BEGIN
    SELECT
        salary
    INTO v_sal
    FROM
        employees
    WHERE
        employee_id = p_id;

    RETURN v_sal; -- RETURN especifica que es lo que la a salir de la funcion
EXCEPTION
    WHEN no_data_found THEN -- Se agrega EXCEPTION por si el ID de empleado no existe
        RETURN NULL; -- Si el ID de empleado no existe se manda un dado nulo
END get_sal;

-- Ejecucion

DECLARE
v_sal employees.salary%type;
BEGIN
v_sal:= get_sal(101); -- Se especifica el parametro (ID de empleado)
dbms_output.put_line('El salario es : '|| v_sal);
END;