--Triggers para cada registro

CREATE OR REPLACE TRIGGER log_emps
AFTER UPDATE OF salary ON employees FOR EACH ROW -- FOR EACH ROW especifica que 
BEGIN                                           -- el trigger se debeb disparar por cada rsgistro
INSERT INTO log_emp_table (ID, SYSDATE)
VALUES (USER, SYSDATE);
END;                                                    