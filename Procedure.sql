/*
PROCEDURE

CREATE [OR REPLACE] PROCEDURE procedure_name
[(parameter1 [mode] datatype1,
parameter2 [mode] datatype2, ...)]
IS|AS
[local_variable_declarations; �]
BEGIN
-- actions;
END [procedure_name];

Utilice CREATE PROCEDURE seguido del nombre, los par�metros opcionales y 
la palabra clave IS o AS.
Agregue la opci�n O REEMPLAZAR para sobrescribir un procedimiento existente.
Escriba un bloque PL / SQL que contenga variables locales, 
un BEGIN y un END (o END nombre_procedimiento).

-Bloques an�nimos: bloques ejecutables sin nombre que no se pueden 
reutilizar sin almacenar para su uso posterior.
-IS o AS: indica la secci�n DECLARE de un subprograma.
-Procedimientos: bloques con nombre que pueden aceptar par�metros 
y se compilan y almacenan en la base de datos.
Subprogramas: bloques PL / SQL con nombre que se compilan 
y almacenan en la base de datos.

*/

CREATE OR REPLACE PROCEDURE add_dept IS
    v_dept_id    departments.department_id%TYPE;
    v_dept_name  departments.department_name%TYPE;
BEGIN
    v_dept_id := 280;
    v_dept_name := 'ST-Curriculum';
    INSERT INTO departments (
        department_id,
        department_name
    ) VALUES (
        v_dept_id,
        v_dept_name
    );
    dbms_output.put_line('Inserted '|| SQL%rowcount || ' row.');
END;

-- Ejecutar Procedimiento
BEGIN
    add_dept;
END;

SELECT
    department_id,
    department_name
FROM
    departments
WHERE
    department_id = 280;






