/*
Paquetes
�Qu� son los paquetes PL / SQL?
Los paquetes PL / SQL son contenedores (objetos de esquema) que le permiten 
agrupar l�gicamente subprogramas, variables, cursores y 
excepciones PL / SQL relacionados.

Un paquete consta de dos partes almacenadas por separado en la base de datos:
-Especificaci�n del paquete: la interfaz para sus aplicaciones.
    -Debe ser creado primero.
    -Declara las construcciones (procedimientos, funciones, variables, etc.) 
        que son visibles para el entorno de llamada.
-Cuerpo del paquete: contiene el c�digo ejecutable de los subprogramas 
que se declararon en la especificaci�n del paquete.
-Tambi�n puede contener sus propias declaraciones variables.

1.- Definicion del paquete
CREATE [OR REPLACE] PACKAGE package_name
IS|AS
    public type and variable declarations
    public subprogram specifications
END [package_name];

2.- Body

CREATE [OR REPLACE] PACKAGE BODY package_name IS|AS
    private type and variable declarations
    subprogram bodies
[BEGIN initialization statements]
END [package_name];

*/
-- Se crea un paquete
-- 1.- Se define el encabezado / especificacion del paquete
CREATE OR REPLACE PACKAGE check_emp_pkg IS
    g_max_length_of_service CONSTANT NUMBER := 100; --Se crea una constante en el paquete
    PROCEDURE chk_hiredate ( -- Se define un procedimiento
        p_date IN employees.hire_date%TYPE
    );
    PROCEDURE chk_dept_mgr ( --Se define un segundo procedimineto
        p_empid  IN  employees.employee_id%TYPE,
        p_mgr    IN  employees.manager_id%TYPE
    );
END check_emp_pkg;

--Los procedimientos no se almacenan dentro del paquete, no estan afuera en las
-- carpetas definidas por oracle

-- 2.- BODY
-- Se especifica el funcionamiento del paquete
CREATE OR REPLACE PACKAGE BODY check_emp_pkg IS
    PROCEDURE chk_hiredate (
        p_date IN employees.hire_date%TYPE
    ) IS
    BEGIN
        IF months_between(sysdate, p_date) > g_max_length_of_service * 12 THEN
            raise_application_error(-20200, 'Invalid Hiredate');
        END IF;
    END chk_hiredate;

PROCEDURE chk_dept_mgr (
    p_empid  IN  employees.employee_id%TYPE,
    p_mgr    IN  employees.manager_id%TYPE
) IS BEGIN 
    DBMS_OUTPUT.PUT(', ');
END chk_dept_mgr;
END check_emp_pkg;
