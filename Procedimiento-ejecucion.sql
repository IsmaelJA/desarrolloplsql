-- Se crea un Procedimiento 

CREATE OR REPLACE PROCEDURE print_date IS
    v_date VARCHAR2(30);
BEGIN
    SELECT
        to_char(sysdate, 'Mon DD, YYYY')
    INTO v_date
    FROM
        dual;

    dbms_output.put_line(v_date);
END;

-- Se ejecuta procedimiento 
BEGIN
PRINT_DATE;
END;