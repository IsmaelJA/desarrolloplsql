--Triggers para cada registro
--:OLD y :NEW muestran valores viejos o posteriores segun las modificaciones 

CREATE OR REPLACE TRIGGER log_emps
AFTER UPDATE OF salary ON employees FOR EACH ROW -- FOR EACH ROW especifica que 
BEGIN                                           -- el trigger se debeb disparar por cada rsgistro
BEGIN
INSERT INTO audit_emp(user_name, time_stamp, id,
old_last_name, new_last_name, old_title,
new_title, old_salary, new_salary)
VALUES (USER, SYSDATE, :OLD.employee_id,
:OLD.last_name, :NEW.last_name, :OLD.job_id,
:NEW.job_id, :OLD.salary, :NEW.salary);
END;                                                  