/*
Triggers
Los TIGGERS permiten que las acciones especificadas se realicen 
AUTOMATICAMENTE dentro de la base de datos, sin tener que escribir 
c�digo de aplicaci�n adicional. Aumentan el poder de la base de datos y el poder 
de su aplicaci�n.

*/

-- Se crea el TRIGGER 
-- Este trigger llamado "log_sal_change_trigg"
-- cada que se actualice el salario en la tabla employees se dispara
-- e inserta los registros USER y SYSDATE en la tabla log_table
CREATE
OR REPLACE TRIGGER log_sal_change_trigg
AFTER
UPDATE OF salary ON employees
BEGIN
INSERT INTO log_table
VALUES (USER, SYSDATE);
END;

-- query para generar la tabla log_table
CREATE TABLE "HR"."LOG_TABLE" 
   (	"USER" VARCHAR2(100) NOT NULL ENABLE, 
	"DATE" DATE, 
	 CONSTRAINT "LOG_TABLE_PK" PRIMARY KEY ("USER", "DATE"))



  
  