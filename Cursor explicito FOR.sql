/*
Cursor explicito con FOR

FOR record_name IN cursor_name LOOP
statement1;
statement2;
. . .
END LOOP;

- record_name Is the name of the implicitly declaredrecord(as cursor_name%ROWTYPE)
- cursor_name Is a PL/SQL identifier for a previouslydeclared cursor
*/

DECLARE
    CURSOR cur_emps IS
    SELECT
        employee_id,
        last_name
    FROM
        employees
    WHERE
        department_id = 50;

BEGIN
    FOR v_emp_record IN cur_emps LOOP
        dbms_output.put_line(v_emp_record.employee_id
                             || ' '
                             || v_emp_record.last_name);
    END LOOP;
END;