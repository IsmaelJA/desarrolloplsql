
-- Muestra el empleado del mes
-- Si existen empleados con el mismo last name, muestra la excepcion 
-- Tambien si no se encentra el usuario genera otra excepcion 
DECLARE 
    v_first_name varchar2(25); --Se declaran variables
    v_last_name VARCHAR2(25); 
BEGIN -- comienza el bloque
    SELECT 
        first_name,
        last_name
    INTO 
        v_first_name,
        v_last_name
    FROM
        employees
    WHERE
        last_name = 'Oswaldo';

    dbms_output.put_line('The employee of the month is: '
                         || v_first_name
                         || ' '
                         || v_last_name
                         || '.');

EXCEPTION
    WHEN too_many_rows THEN -- Si hay mas de un empleado con el last name "Oswaldo" lanza la excepcion
        dbms_output.put_line('Your select statement retrieved
multiple rows. Consider using a cursor or changing
the search criteria.');
    when no_data_found then 
    dbms_output.put_line('No se encontro usuario.');
END;