/*
Paquetes - Funciones globales y locales

-Componentes privados: se declaran solo en el BODY del paquete 
y solo pueden hacer referencia a ellos otras 
construcciones (públicas o privadas) dentro del mismo cuerpo del paquete.

-Componentes públicos: se declaran en la especificación del paquete y se 
pueden invocar desde cualquier entorno de llamada, siempre que se 
haya otorgado al usuario el privilegio EXECUTE en el paquete.

Visibilidad: describe si un componente puede ser visto, es decir, 
referenciado y utilizado por otros componentes u objetos.
*/

-- Las funciones globales se definen dentro del BODY
-- Las funciones locales se definen en la estructura del paquete

-- Se define el paquete 
CREATE OR REPLACE PACKAGE salary_pkg
IS g_max_sal_raise CONSTANT NUMBER := 0.20;
PROCEDURE update_sal 
(p_employee_id employees.employee_id%TYPE,
p_new_salary employees.salary%TYPE);
END salary_pkg;

-- BODY
CREATE OR REPLACE PACKAGE BODY salary_pkg IS
FUNCTION validate_raise -- Funcion privada (esta definida dentro del BODY)
(p_old_salaryemployees.salary%TYPE,
p_new_salary employees.salary%TYPE)
RETURN BOOLEAN IS
BEGIN
IF p_new_salary > (p_old_salary * (1 + g_max_sal_raise)) THEN
RETURN FALSE;
ELSE
RETURN TRUE;
END IF;
END validate_raise;
PROCEDURE update_sal -- Procedimiento Global
(p_employee_id employees.employee_id%TYPE,
p_new_salary employees.salary%TYPE)
IS v_old_salary employees.salary%TYPE ;--local variable BEGIN
SELECT salary INTO v_old_salary 
FROM employees
WHERE employee_id = p_employee_id;
IF validate_raise(v_old_salary, p_new_salary) THEN
UPDATE employees
SET salary = p_new_salary
WHERE employee_id = p_employee_id;
ELSE
RAISE_APPLICATION_ERROR(-20210, 'Aumento demasiado alto');
END IF;
END update_sal;
END salary_pkg;