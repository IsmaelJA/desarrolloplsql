/*
Record (Registro): un tipo de datos compuesto que consiste en un grupo 
de elementos de datos relacionados almacenados como campos, 
cada uno con su propio nombre y tipo de datos.

TYPE keyword to define your record structure.

TYPE type_name IS RECORD
(field_declaration[,field_declaration]...);
identifier type_name;
*/

declare type person_deptis record 
    (first_name       employees.first_name%TYPE,
    last_name        employees.last_name%TYPE,
    department_name  departments.department_name%TYPE);
    v_person_dept_rec person_dept;
BEGIN
    SELECT
        e.first_name,
        e.last_name,
        d.department_name
    INTO v_person_dept_rec
    FROM
        employees e JOIN departments d ON e.department_id = d.department_id
    WHERE
        employee_id = 200;
        dbms_output.put_line(v_person_dept_rec.first_name
                         || ' '
                         || v_person_dept_rec.last_name
                         || ' is in the '
                         || v_person_dept_rec.department_name
                         || ' department.');
END;

