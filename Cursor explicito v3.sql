DECLARE
    CURSOR cur_emps IS
    SELECT *
    FROM
        employees
    WHERE
        department_id = 30;

    v_emp_record cur_emps%rowtype; --Variable de el tipo cursor
-- Esta variable se define para no definir uchas variables para muchos registros     
    
BEGIN
    OPEN cur_emps;
    LOOP
        FETCH cur_emps INTO v_emp_record;
        EXIT WHEN cur_emps%notfound;
        dbms_output.put_line(v_emp_record.employee_id
                             || ' - '
                             || v_emp_record.last_name);
                             
                             /*Para especificar el campo que se necesita, como la variable tiene la misma
                             estructura del cursor y a su vez e cursor tiene a misma estructura que la tabla, 
                             se especifica por el nombre de la variable + . + el nombre del campo */
    END LOOP;

    CLOSE cur_emps;
END;