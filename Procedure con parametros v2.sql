/*
Procedimientos con parametros

OUT especifica que el parametro es de salida
*/

CREATE OR REPLACE PROCEDURE query_emp
(p_id IN employees.employee_id%TYPE,
p_name OUT employees.last_name%TYPE,
p_salary OUT employees.salary%TYPE) IS
BEGIN
SELECT last_name, salary 
INTO p_name, p_salary
FROM employees
WHERE employee_id = p_id;
END query_emp;

---- Ejecucion
DECLARE 
a_emp_name employees.last_name%TYPE;
a_emp_sal employees.salary%TYPE;
BEGIN
query_emp(a_emp_name, a_emp_sal,p_id=>111); -- Aqui espera los parametros de salida
DBMS_OUTPUT.PUT_LINE('Nombre: ' || a_emp_name);
DBMS_OUTPUT.PUT_LINE('Salario: ' || a_emp_sal);
END;