/*
Una colecci�n PL / SQL es un conjunto con nombre de muchas ocurrencias 
del mismo tipo de datos almacenados como una variable.
Una colecci�n es un tipo de variable compuesta, similar a los registros 
definidos por el usuario.

Una tabla INDEX BY, que se basa en un solo campo o columna; 
por ejemplo, la columna last_name de la tabla EMPLOYEES.

Una tabla de registros INDEX BY, que se basa en un tipo de registro compuesto; 
por ejemplo, la estructura de filas en la tabla DEPARTMENTS.

INDEX BY table debe tener una clave primaria que sirva como �ndice de los datos.

TYPE type_nameIS TABLE OF DATA_TYPE
INDEX BY PRIMARY_KEY_DATA_TYPE;
identifier type_name;
*/

TYPE t_hire_dateIS TABLE OFDATE
INDEX BY BINARY_INTEGER;
v_hire_date_tab t_hire_date;

-- Implementado
DECLARE
TYPE t_hire_dateIS TABLE OF employees.hire_date%TYPE
INDEX BY BINARY_INTEGER;
v_hire_date_tab t_hire_date;
v_count BINARY_INTEGER := 0;
BEGIN
FOR emp_rec IN
(SELECT hire_dateFROM employees)
LOOP
v_count := v_count + 1;
v_hire_date_tab(v_count) := emp_rec.hire_date;
END LOOP;
END;