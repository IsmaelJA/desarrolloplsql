/*
Procedura con parametros

IN OUT el parametro es de entrada y salida 
*/

CREATE OR REPLACE PROCEDURE format_phone (
    p_phone_no in OUT VARCHAR2
) IS
BEGIN
    p_phone_no := '('
                  || substr(p_phone_no, 1, 3)
                  || ')'
                  || substr(p_phone_no, 4, 3)
                  || '-'
                  || substr(p_phone_no, 7);
END format_phone;

-----Ejecucion

DECLARE
    a_phone_no VARCHAR2(13);
BEGIN
    a_phone_no := '5525189513';
    format_phone(a_phone_no);
    dbms_output.put_line('El formato del numero es : ' || a_phone_no);
END;





