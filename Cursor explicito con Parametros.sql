--Cursor con parametro

DECLARE 
cursor cur_country (p_region_id number) is --Se e define un parametro al cursor
SELECT
    country_id,
    country_name
FROM
    countries
WHERE
    region_id = p_region_id; --Aqui lee el parametro
    v_country_record cur_country%rowtype;

BEGIN
    OPEN cur_country(1); -- Aqui le asigna el parametro
    LOOP
        FETCH cur_country INTO v_country_record;
        EXIT WHEN cur_country%notfound;
        dbms_output.put_line(v_country_record.country_id
                             || '' || v_country_record.country_name);
    END LOOP;
    CLOSE cur_country;
END;