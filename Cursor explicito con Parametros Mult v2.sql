-- Cursor con parametros multiples

DECLARE
    CURSOR cur_emps (p_job     VARCHAR2,p_salary  NUMBER) IS --Parametro en el cursor
    SELECT
        employee_id,
        last_name
    FROM
        employees
    WHERE
            job_id = p_job
        AND salary > p_salary;

BEGIN
    FOR v_emp_record IN cur_emps('IT_PROG', 10000) LOOP -- Parametro en FOR
        dbms_output.put_line(v_emp_record.employee_id
                             || ' '
                             || v_emp_record.last_name);
    END LOOP;
END;