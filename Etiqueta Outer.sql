-- Guardar registros en variables 
    --v_emp_record employees%rowtype;

/*
A travez del OUTER se llaga al bloque externo
*/
<< externo >> DECLARE
    v_father_name    VARCHAR2(20) := 'Patrick';
    v_date_of_birth  DATE := '20-Abril-1972';
BEGIN
    DECLARE
        v_child_name     VARCHAR2(20) := 'Mike';
        v_date_of_birth  DATE := '12-Diciembre-2002';
    BEGIN
        dbms_output.put_line('Father''sName: ' || v_father_name);
        dbms_output.put_line('Date of Birth: ' || externo.v_date_of_birth);
                                                -- outer. llega al bloque externo
        dbms_output.put_line('Child''sName: ' || v_child_name);
        dbms_output.put_line('Date of Birth: ' || v_date_of_birth);
    END;
END;