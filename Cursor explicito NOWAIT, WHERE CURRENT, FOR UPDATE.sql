/* 
NOWAIT le dice al servidor Oracle que no espere si alguna de las 
filas solicitadas ya ha sido bloqueada por otro usuario.
Si omite la palabra clave NOWAIT, el servidor Oracle espera 
indefinidamente hasta que las filas est�n disponibles.

WHERE CURRENT OF se usa junto con la cl�usula FOR UPDATE 
para referirse a la fila actual (la fila FETCHed m�s reciente) 
en un cursor expl�cito.

FOR UPDATE desbloquea las filas primero.

WHERE CURRENT OF hace referencia a la fila actual en un cursor expl�cito.
*/

DECLARE
    cursor cur_emps is
    SELECT
        employee_id,
        salary
    FROM
        my_employees
    WHERE
        salary <= 20000 FOR UPDATE NOWAIT;
        v_emp_rec cur_emps%rowtype;
BEGIN
    OPEN cur_emps;
    LOOP
        FETCH cur_emps INTO v_emp_rec;
        EXIT WHEN cur_emps%notfound;
        UPDATE my_employees
        SET salary = v_emp_rec.salary * 1.1
        WHERE CURRENT OF cur_emps;
    END LOOP;
    CLOSE cur_emps;
END;