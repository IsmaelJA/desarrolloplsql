-- SQL Dinamico
-- Proporciona la capacidad de ejecutar sentencias 
    -- SQL cuya estructura es desconocida hasta el momento de la ejecución.



CREATE FUNCTION eliminar_registros( p_nombre_tabla VARCHAR2)
    RETURN NUMBER IS 
    BEGIN 
    EXECUTE IMMEDIATE 'DELETE FROM ' || p_nombre_tabla;
    return sql%rowcount;
END;

--- Ejecucion
DECLARE
    v_count NUMBER;
BEGIN
    v_count := eliminar_registros('COPY_EMPLOYEES');
    dbms_output.put_line(v_count || ' registros borrados.');
END;
